﻿import { KeycloakService } from 'keycloak-angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from './_models';
import { KeycloakProfile } from 'keycloak-js';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent implements OnInit{
    userDetails: KeycloakProfile;

    constructor(private keycloakService: KeycloakService) {}

    async ngOnInit() {
      if (await this.keycloakService.isLoggedIn()) {
        this.userDetails = await this.keycloakService.loadUserProfile();
      }
    }

    async doLogout() {
      await this.keycloakService.logout();
    }
}
