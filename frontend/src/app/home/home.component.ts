﻿import { KeycloakService } from 'keycloak-angular';
import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { HelloService } from '../_services/hello.service';
import { HelloMessage } from '../_models/helloMessage';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    helloMessage: HelloMessage;
    userName: string;
    private httpHeaders: HttpHeaders;


    constructor(private helloService: HelloService, private keycloak: KeycloakService) {
    }

    ngOnInit() {
        this.loading = true;
        this.userName = this.keycloak.getUsername();

        this.helloService.get().subscribe(helloMessage => {
          this.helloMessage = helloMessage;
          this.loading = false;
        });
    }
}
