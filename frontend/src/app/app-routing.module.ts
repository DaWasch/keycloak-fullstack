import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { AuthGuard } from '@app/auth-guard';
import { IndexComponent } from './index/index.component';

const routes: Routes = [
    { path: 'hello', component: HomeComponent , canActivate: [AuthGuard], data: {roles: ['app-user']} },
    { path: '', component: IndexComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [AuthGuard]
})
export class AppRoutingModule { }
