﻿import { APP_INITIALIZER,NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { environment } from '../environments/environment';;
import { IndexComponent } from './index/index.component'

// tslint:disable-next-line: typedef
function initializeKeycloak(keycloak: KeycloakService): () => Promise<any>  {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      const keycloakConfig = {
        url: environment.authUrl,
        realm: environment.authRealm,
        clientId: environment.authApplication,
      };
      try {
        await keycloak.init({
          config: keycloakConfig,
          loadUserProfileAtStartUp: true,
          initOptions: {
            onLoad: 'check-sso',
            silentCheckSsoRedirectUri:
              window.location.origin + '/assets/silent-check-sso.html',
          },
          bearerExcludedUrls: []
        });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };
}

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        KeycloakAngularModule
    ],
    declarations: [
        AppComponent,
        HomeComponent
,
        IndexComponent    ],
    providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: initializeKeycloak,
          multi: true,
          deps: [KeycloakService],
        },

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
