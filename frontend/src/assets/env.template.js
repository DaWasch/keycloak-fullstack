(function(window) {
  window.env = window.env || {};

  // Environment variables
  window['env']['apiUrl'] = 'https://'+'${API_URL}';
  window['env']['authUrl'] = '${KEYCLOAK_AUTH_SERVER_URL}';
  window['env']['authRealm'] = '${KEYCLOAK_REALM}';
  window['env']['authApplication'] = '${KEYCLOAK_RESOURCE}';
})(this);
