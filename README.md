# Angular/Spring Boot/Keycloak/Docker Authentication Example

This example shows one possibilty to implement an Angular frontend and Spring Boot Backend using Keycloak for authentication. 

## Prerequisites
1. Keycloak up an running (See keycloak-docker-compose)
2. Configure Keycloak having a client called "fullstack-app", a client role "app-user" and a user owning this role
3. Configure Keycloak client "fullstack-app" and add following redirect urls
   1. http://localhost:4200/* (Angular ng serve url)
   2. http://localhost:8080/* (Spring Boot backend url)
   3. http://localhost:9080/* (Angular frontend url when deployed with given docker-compose-local.yml)
   3. http://localhost:8900/* (Spring Boot bakcend url when deployed with given docker-compose-local.yml)
4. Configure web origins corrospondingly 
   1. http://localhost:4200 (Angular ng serve url)
   2. http://localhost:8080 (Spring Boot backend url local build)
   3. http://localhost:9080 (Angular frontend url when deployed with given docker-compose-local.yml)
   3. http://localhost:8900 (Spring Boot bakcend url when deployed with given docker-compose-local.yml)
5. Jenkinsfile is for optional use with Jenkins Muilti Branch Pipeline