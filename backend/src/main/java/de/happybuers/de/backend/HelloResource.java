package de.happybuers.de.backend;

import de.happybuers.de.backend.model.HelloMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@Slf4j
@RolesAllowed("app-user")
@CrossOrigin
public class HelloResource {

    @GetMapping("/hello")
    public ResponseEntity<HelloMessage> hello() {
        log.info("open /hello");
        return ResponseEntity.ok(new HelloMessage("Your are now sucessfully locked in and can read this message from the server!"));
    }
}
