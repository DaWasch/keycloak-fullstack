package de.happybuers.de.backend.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthenticationResponse {
    Long id;
    String username;
    String firstname;
    String lastname;
    String token;
}
